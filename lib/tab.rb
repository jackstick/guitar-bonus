require 'pry-byebug'

class Tab
  def initialize(tab_str)
    @tab_str = tab_str
  end

  def string_notes
    result = []
    @tab_str.split("\n\n").each do |large_row|
      notes = parse_single_large_row(large_row)
      result.push((notes.map {|note| parse_note(note)}.flatten()).join(' '))
    end
    result.join(' ')
  end

  def parse_single_large_row(large_row)
    (large_row.split("\n").map {|row| parse_row(row.split('|')[0], remove_delimiters(row))}).transpose
  end

  def remove_delimiters(row)
    row.slice(2..-2).gsub(/\|-/, '')
  end

  def parse_row(stringLetter, row)
    row.gsub(/.(.)?/, '\1').split('').map! {|el| (el!='-'?(stringLetter+el):el)}
  end

  def parse_note(note)
    note.count('-')==6? '_': (note.delete_if {|el| el=='-'}).join('/')
  end
end