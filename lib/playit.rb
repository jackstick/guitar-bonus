require_relative './guitar'
require_relative './midi_guitar'
require_relative './tab'

if __FILE__ == $0
  tabstr = """
  e|-------------------------------|
  B|-5-5-6-8-8-6-5-3-1-1-3-5-5-3-3-|
  G|-------------------------------|
  D|-------------------------------|
  A|-------------------------------|
  E|-------------------------------|
"""
  Guitar.new(MidiGuitar.new).play(Tab.new(tabstr))
end
