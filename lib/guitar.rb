class Guitar
  attr_accessor :midi_guitar
  def initialize(midi_guitar)
    @midi_guitar = midi_guitar
  end

  def play(tab)
    @midi_guitar.play(tab.string_notes)
  end
end