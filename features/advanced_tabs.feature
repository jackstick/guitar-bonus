Feature: Translation of guitar tabs
    As a music fan
    I would like to convert guitar tabs to music
    So that I can hear what they sound like

  Scenario: Playing Stairway to Heaven (multiple strings)
	Given I start my application
	And I load the following guitar tab
	  """
	  e|-------5-7-----7-|
	  B|-----5-----5-----|
	  G|---5---------5---|
	  D|-7-------6-------|
	  A|-----------------|
	  E|-----------------|
      """
	When the guitar app parses the tab above
	Then the following notes should be generated
	  """
	  D7 G5 B5 e5 e7/D6 B5 G5 e7
	  """

  Scenario: Playing Stairway to Heaven (silences and divisions)
	Given I start my application
	And I load the following guitar tab
	  """
	  e|-------5-7-----7-|-8-----8-2-----2-|-0---------0-----|-----------------|
	  B|-----5-----5-----|---5-------3-----|---1---1-----1---|-0-1-1-----------|
	  G|---5---------5---|-----5-------2---|-----2---------2-|-0-2-2-----------|
	  D|-7-------6-------|-5-------4-------|-3---------------|-----------------|
	  A|-----------------|-----------------|-----------------|-2-0-0---0---8-7-|
	  E|-----------------|-----------------|-----------------|-----------------|
      """
	When the guitar app parses the tab above
	Then the following notes should be generated
	  """
	  D7 G5 B5 e5 e7/D6 B5 G5 e7 e8/D5 B5 G5 e8 e2/D4 B3 G2 e2 e0/D3 B1 G2 B1 _ e0 B1 G2 B0/G0/A2 B1/G2/A0 B1/G2/A0 _ A0 _ A8 A7
	  """

  Scenario: Playing Stairway to Heaven (multiple tab segments)
	Given I start my application
	And I load the following guitar tab
	  """
	  e|-------5-7-----7-|-8-----8-2-----2-|-0---------0-----|-----------------|
	  B|-----5-----5-----|---5-------3-----|---1---1-----1---|-0-1-1-----------|
	  G|---5---------5---|-----5-------2---|-----2---------2-|-0-2-2-----------|
	  D|-7-------6-------|-5-------4-------|-3---------------|-----------------|
	  A|-----------------|-----------------|-----------------|-2-0-0---0---8-7-|
	  E|-----------------|-----------------|-----------------|-----------------|

	  e|-----------7-----7-|-8-----8-2-----2-|-0---------0-----|-----------------|
	  B|---------5---5-----|---5-------3-----|---1---1-----1---|-0-1-1-----------|
	  G|-------5-------5---|-----5-------2---|-----2---------2-|-0-2-2-----------|
	  D|-----7-----6-------|-5-------4-------|-3---------------|-----------------|
	  A|-0-----------------|-----------------|-----------------|-2-0-0-------0-2-|
	  E|-------------------|-----------------|-----------------|-----------------|
      """
	When the guitar app parses the tab above
	Then the following notes should be generated
	  """
	  D7 G5 B5 e5 e7/D6 B5 G5 e7 e8/D5 B5 G5 e8 e2/D4 B3 G2 e2 e0/D3 B1 G2 B1 _ e0 B1 G2 B0/G0/A2 B1/G2/A0 B1/G2/A0 _ A0 _ A8 A7 A0 _ D7 G5 B5 e7/D6 B5 G5 e7 e8/D5 B5 G5 e8 e2/D4 B3 G2 e2 e0/D3 B1 G2 B1 _ e0 B1 G2 B0/G0/A2 B1/G2/A0 B1/G2/A0 _ _ _ A0 A2
	  """
