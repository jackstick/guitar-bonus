require 'cucumber/rspec/doubles'
require 'guitar'
require 'midi_guitar'
require 'tab'

When(/^the guitar app parses the tab above$/) do
  @guitar.play(@tab)
end
