require 'cucumber/rspec/doubles'
require 'guitar'
require 'midi_guitar'
require 'tab'

Then(/^the following notes should be generated$/) do |tabstr|
  expect(@midi_guitar).to have_received(:play).with(tabstr)
end